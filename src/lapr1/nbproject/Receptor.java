package lapr1.nbproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Classe Receptor: faz a descodificação da mensagem
 * @author axpppp
 */

public class Receptor {
    
    // objecto ler to do tipo Scanner como static (para poder ser usado em toda a classe)
    static Scanner ler = new Scanner(System.in);
    
    // variável de classe que guarda mensagem codificada
    private static String msgCodificada = "";
    // variável de classe para matriz codificador, inicializada com todos os valores a -1 (útil para o imprimir matriz)
    private static int[][] matrizCod = new int[][]{{-1,-1,-1,-1}, {-1,-1,-1,-1}, {-1,-1,-1,-1}, {-1,-1,-1,-1}};
    // variável de classe para a ordem da matriz
    private static int ordem;
    // variável de classe que guardar os caracteres da tabela conversora (com capacidade para 200)
    private static char[] tabelaConvChars = new char[200];
    // variável de classe que guardar o código de caracteres da tabela conversora (com capidade para 200)
    private static int[] tabelaConvCodigo = new int[200];
    // variável de classe que representa a matriz identidade
    private static double[][] ident = new double[][]{{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {0,0,0,1}};
    // variável de classe que representa a matriz inversa
    private static double[][] inversa = new double[4][4];
    // variável de classe que indica se já há matriz inversa
    private static boolean existeInv = false;
    // variável de classe para guardar mensagem descodificada
    private static String msgDescod = "";
    // variável de classe que representa uma generalização da matriz adjunta
    private static String[][] adjuntaGenerica = new String[][]{{"|C11|","|C12|","|C13|","|C14|"}, {"|C21|","|C22|","|C23|","|C24|"}, {"|C31|","|C32|","|C33|","|C34|"}, {"|C41|","|C42|","|C43|","|C44|"}};
    // variáveis para verificar se pode exportar para html
    private static boolean identHtml = false;
    private static boolean adjHtml = false;
    private static double[][] inversaHtml = new double[4][4];
    
    
    /**
     * Metodo Main, que invoca os Menus para escolher o que se quer fazer.  
     */
    public static void main(String[] args) throws FileNotFoundException {
        welcome();
    } // end main

    /**
     * Menu de boas-vindas da aplicação receptora
     */
    public static void welcome() throws FileNotFoundException{
        System.out.println("****************************************************");
        System.out.println("*                                                  *");
        System.out.println("* ISEP - INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO *");
        System.out.println("*                                                  *");
        System.out.println("*   Departamento de Engenharia Informática         *");
        System.out.println("*               LAPR1 - 2012-2013                  *");
        System.out.println("*                                                  *");
        System.out.println("*                                                  *");
        System.out.println("*          <-- Aplicação Receptora -->             *");
        System.out.println("*                                                  *");
        System.out.println("*                                                  *");
        System.out.println("*       Autores:                                   *");
        System.out.println("*           João Carreira                          *");
        System.out.println("*           Paulo Ponciano                         *");
        System.out.println("*                                                  *");															  
        System.out.println("*                                   Dezembro 2012  *");
        System.out.println("*                                                  *");
        System.out.println("****************************************************");
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*                  Correr programa (c) / Sair (s)  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            // guarda opção 
            String opcao = ler.nextLine().toLowerCase();
            // converte para char (considera apenas 1º caracter)
            char opcao2 = opcao.charAt(0);
            if(opcao2 == 'c'){
                menuReceptor();
                sair = true;
            }
            else if(opcao2 == 's'){
                System.out.println("Programa terminado!");
                sair = true;
            }
            else
                System.out.println("Opção inválida!");
        } // end while 
    } // end welcome
    
    
    /**
     * Menu da aplicação emissora
     */
    public static void menuReceptor() throws FileNotFoundException{
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*               Aplicação Receptora                *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     1 - Ler mensagem de HTML                     *");
            System.out.println("*     2 - Mostrar mensagem (matriz)                *");
            System.out.println("*     3 - Ler matriz codificadora de HTML          *");
            System.out.println("*     4 - Mostrar matriz codificadora              *");
            System.out.println("*     5 - Ler tabela conversora de HTML            *");
            System.out.println("*     6 - Mostrar tabela conversora                *");
            System.out.println("*     7 - Calcular e escrever matriz inversa       *");
            System.out.println("*         (usando matriz identidade)               *");
            System.out.println("*     8 - Calcular e escrever matriz inversa       *");
            System.out.println("*         (usando matriz adjunta)                  *");
            System.out.println("*     9 - Mostrar texto descodificado              *");
            System.out.println("*                                                  *");
            System.out.println("*     S - Sair                                     *");															  
            System.out.println("*                                                  *");
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = ler.nextLine().toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case '1':
                    lerMsgHtml();
                    break;
                case '2':
                    mostrarMsgCodificada();
                    break;
                case '3':
                    lerMatrizHTML();
                    break;
                case '4':
                    mostrarMatriz();
                    break;
                case '5':
                    lerTabelaConvHTML();
                    break;
                case '6':
                    mostrarTabelaConv();
                    break;
                case '7':
                    if(Emissor.existeMatrizCod(ordem))
                        calcMatrizInvIdent();
                    else
                        System.out.println("Não existe nenhuma matriz carregada na aplicação!");
                    break;
                case '8':
                     if(ordem == 2){
                         System.out.println("Não é possível calcular pela matriz adjunta por que a matriz carregada no sistema é de ordem igual a 2!\n Carregue uma matriz de ordem 3 ou 4 e tente novamente!");
                     }
                     else{
                        if(Emissor.existeMatrizCod(ordem))
                            calcMatrizInvAdj();
                        else
                            System.out.println("Não existe nenhuma matriz carregada na aplicação!");
                     }
                    break;
                case '9':
                        mostrarTexto();
                    break;
                case 's':
                    System.out.println("Programa terminado!");
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
            
        } // end while
    } // end menuReceptor
    
    
    /**
     * método para calcular e mostrar a matriz inversa, na consola ou exportando
     * para um ficheiro HTML, usando a matriz identidade
     */
    public static void calcMatrizInvIdent() throws FileNotFoundException{
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*       7. Calcular matriz inversa                 *");
            System.out.println("*       (usando matriz identidade)                 *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     C - Mostrar na consola                       *");
            System.out.println("*     H - Exportando para HTML                     *");
            System.out.println("*     V - Voltar ao menu anterior                  *");															  
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = ler.nextLine().toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case 'c':
                    matrizInversaIdent();
                    sair = true;
                    break;
                case 'h':
                    matrizInversaIdentHtml();
                    sair = true;
                    break;
                case 'v':
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
        } // end while
    } // end calcMatrizInvIdent

    
    /**
     * método para calcular e mostrar a matriz inversa, na consola ou exportando
     * para um ficheiro HTML, usando a matriz adjunta
     */
    public static void calcMatrizInvAdj() throws FileNotFoundException{
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*       8. Calcular matriz inversa                 *");
            System.out.println("*       (usando matriz adjunta)                    *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     C - Mostrar na consola                       *");
            System.out.println("*     H - Exportando para HTML                     *");
            System.out.println("*     V - Voltar ao menu anterior                  *");															  
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = ler.nextLine().toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case 'c':
                    matrizInversaAdj();
                    sair = true;
                    break;
                case 'h':
                    matrizInversaAdjHtml();
                    sair = true;
                    break;
                case 'v':
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
        } // end while
    } // end calcMatrizInvAdj

    
    /**
     * método para calcular e mostrar a matriz inversa, na consola ou exportando
     * para um ficheiro HTML, usando a matriz adjunta
     */
    public static void mostrarTexto() throws FileNotFoundException{
        // verifica se estão reunidas todas as condições para mostrar o texto
        boolean msg = Emissor.existeMensagem(msgCodificada);
        boolean matriz = Emissor.existeMatrizCod(ordem);
        boolean tabela = Emissor.existeTabelaConv(tabelaConvChars, tabelaConvCodigo);
        // se não existir algum dos elementos anteriores, avisa o utilizador
        if(!msg || !matriz || !tabela || !existeInv){
            System.out.println("Não é possível prosseguir! Motivo(s): ");
            if(!msg)
                System.out.println("Não existe mensagem para descodificar!");
            if(!matriz)
                System.out.println("Não existe matriz codificadora!");
            if(!tabela)
                System.out.println("Não existe tabela conversora!");
            if(!existeInv)
                System.out.println("Não foi calculada matriz inversa!");
        } // end if
        else{
            boolean sair = false;
            while(!sair){
                System.out.println("****************************************************");
                System.out.println("*                                                  *");
                System.out.println("*       9. Mostrar texto descodificado             *");
                System.out.println("*                                                  *");
                System.out.println("****************************************************");
                System.out.println("*                                                  *");
                System.out.println("*     C - Mostrar na consola                       *");
                System.out.println("*     T - Exportando para ficheiro de texto        *");
                System.out.println("*     V - Voltar ao menu anterior                  *");															  
                System.out.println("*                                                  *");
                System.out.println("*--------------------------------------------------*");
                System.out.println("*                                                  *");
                System.out.println("*               Escolha uma opção                  *");
                System.out.println("*                                                  *");
                System.out.println("****************************************************");
                String opcao = ler.nextLine().toLowerCase();
                char opcao2 = opcao.charAt(0);
                switch(opcao2){
                    case 'c':
                        descodificar();
                        mostrarTextoConsola();
                        sair = true;
                        break;
                    case 't':
                        descodificar();
                        escreveMsgTxt();
                        sair = true;
                        break;
                    case 'v':
                        sair = true;
                        break;
                    default:
                        System.out.println("Introduza uma opção válida!");
                } // end switch
            } // end while
        } // end else
    } // end mostrarTexto
    
    
    /**
     * lê a mensagem codificada que se encontra em ficheiro html
     * @throws FileNotFoundException 
     */
    public static void lerMsgHtml() throws FileNotFoundException{
        File f = new File("mensagemCodificada.html");
        if(!f.exists())
            System.out.println("Não existe nenhuma mensagem codificada!");
        else{
            DescodificadorHTML.lerMsgCodificada("mensagemCodificada.html");
            System.out.println("Mensagem lida com sucesso!");
            importMsgCodificada();
        }
    } // end lerMsgHtml
    
    
    /**
     * imprime no ecrã a mensagem codificada
     */
    public static void mostrarMsgCodificada(){
        if(msgCodificada == "")
            System.out.println("Não existe nenhuma mensagem codificada no sistema!");
        else
            System.out.println(msgCodificada);
    } // end mostrarMsgCodificada
    
    
    /**
     * guarda em msgCodificada a mensagem codificada presente na classe DescodificadorHTML
     */
    public static void importMsgCodificada(){
        msgCodificada = DescodificadorHTML.getMsgCodificada();
    }
    
    
    /**
     * lê matriz codificadora a partir de ficheiro HTML
     * @throws FileNotFoundException 
     */
    public static void lerMatrizHTML() throws FileNotFoundException{
        File f = new File("matrizCodificadora.html");
        if(!f.exists())
            System.out.println("Não existe nenhuma matriz codificadora!");
        else{
            DescodificadorHTML.lerMatriz("matrizCodificadora.html");
            System.out.println("Matriz lida com sucesso!");
        }
    } // end lerMatrizHTML
    
    
    /**
     * importa valores da matriz lida a partir de ficheiro HTML
     * @param str string com valores da matriz
     * @param o ordem da matriz
     */
    public static void importMatriz(String str, int o){
        // faz split por espaços em branco
        String[] temp = str.split(" ");
        // variável contadora para controlar as posições de String[] temp
        int c = 0;
        // percorre toda a matriz codificadora
        for(int i = 0; i < o; i++){
           for(int j = 0; j < o; j++){
               // preenche cada posição da matriz com elemento respectivo
               matrizCod[i][j] = Integer.parseInt(temp[c]);
               // incrementa contador
               c++;
           } // end for
        } // end for
        ordem = o;
    } // end importMatriz
    
    
    /**
     * imprime matriz no ecrã
     */
    public static void mostrarMatriz(){
        if(Emissor.existeMatrizCod(ordem))
            Emissor.imprimeMatriz(matrizCod, ordem, ordem);
        else
            System.out.println("Não existe nenhum matriz codificadora carregada no sistema!");
    } // end mostrarMatriz
    
    
    /**
     * lê tabela conversora de caracteres a partir de ficheiro html
     * @throws FileNotFoundException 
     */
    public static void lerTabelaConvHTML() throws FileNotFoundException{
        File f = new File("tabelaConversora.html");
        if(!f.exists())
            System.out.println("Não existe nenhuma tabela conversora de caracteres!");
        else{
            DescodificadorHTML.lerTabelaConv("tabelaConversora.html");
            System.out.println("Tabela conversora de caracteres lida com sucesso!");
        }
    } // end lerTabelaConvHTML
    
    
    /**
     * carrega em tabelaConvChars e tabelaConvCodigo os dados recolhidos a partir
     * do ficheiro de html com tabela conversora de caracteres
     * @param car string contendo os caracteres
     * @param cod string contendo os códigos
     */
    public static void importTabelaConv(String car, String cod){
        // contador "especial" para a string de caracteres (não se pode fazer split() seja de que caracter fôr já que, caso esse caracter esteja na tabela, ele seria perdido)
        int contCar = 0;
        // aqui já é seguro fazer split
        String[] temp = cod.split(" ");
        // variável contadora "normal"
        int cont = 0;
        // ciclo para percorrer ambos os arrays que vão guardar dados da tabela convesora
        for(int i = 0; i < temp.length; i++){
            // no array de chars guarda o caracter da string car com base no contador "especial"
            tabelaConvChars[i] = car.charAt(contCar);
            // no array de códigos guarda o respectivo código com base no contador "normal"
            tabelaConvCodigo[i] = Integer.parseInt(temp[cont]); 
            // incrementa os respectivos contadores
            cont++;
            // o contador "especial" tem que ser incrementado por 2 unidades dado que na string car os vários caracteres estão nas posições 0, 2, 4, 6, ..., etc
            contCar += 2;
        } // end for
    } // end importTabelaConv
    
    
    /**
     * imprime tabela conversora de caracteres no ecrã
     */
    public static void mostrarTabelaConv(){
        if(tabelaConvChars[0] == 0)
            System.out.println("Não existe nenhuma tabela conversora de caracteres carregada no sistema!");
        else{
            System.out.println("Caracter - Codigo");
            for(int i = 0; i < 200; i++){
                if(tabelaConvChars[i] != 0){
                    System.out.printf("%4c %9d\n", tabelaConvChars[i], tabelaConvCodigo[i]);
                }
            } // end for
        } // end else
    } // end mostrarTabelaConv
    
    
    /**
     * calcula a matriz inversa recorrendo à matriz identidade, pelo método da
     * condensação
     */
    public static void matrizInversaIdent(){
        // número de passos
        int cont = 1;
        // declarar matriz de floats e criá-la a partir de matrizCod
        double[][] matriz = matrizToDouble(matrizCod, ordem);
        // imprime matrizes antes de se efectuarem operações
        System.out.println("-> Passos detalhados de resolução pelo método da condensação\n");
        System.out.printf("Passo 0 (antes de qualquer operação)\n");
        imprimeMatrizMetCond(matriz,ident,ordem,ordem);
        // percorrer diagonal inferior da matriz
        for(int i = 0; i < ordem; i++){
            // esta condição garante que só se percorre diagonal inferior
            for(int j = 0; j < i; j++){
                // efectua cálculo se fôr diferente de 0
                if(matriz[i][j] != 0){
                    System.out.printf("\n\nPasso número %d (na diagonal inferior): \n",cont);
                    colocaZero(matriz,i,j);               
                    imprimeMatrizMetCond(matriz,ident,ordem,ordem);
                    cont++;
                }
            } // end for
        } // end for
        // percorrer diagonal superior da matriz
        for(int i = 0; i < ordem; i++){
            // esta condição garante que só se percorre diagonal superior
            for(int j = i + 1; j < ordem; j++){
                // efectua cálculo se fôr diferente de 0
                if(matriz[i][j] != 0) {
                    System.out.printf("\n\nPasso número %d (na diagonal superior): \n",cont);
                    colocaZero(matriz,i,j);
                    imprimeMatrizMetCond(matriz,ident,ordem,ordem);
                    cont++;
                } // end if
            } // end for
        } // end for
        reduzirDiagonal(matriz);
        System.out.printf("\n\nPasso número %d (reduzir diagonal): \n",cont);
        imprimeMatrizMetCond(matriz,ident,ordem,ordem);
        // copia actuais valores da matriz identidade para a matriz inversa
        guardarInversa(ident);
        // passa boolean para true
        existeInv = true;
        // faz reset à matriz identidade de modo a que esta possa ser usada de novo
        resetIdent();
    } // end calcMatrizInversaIdent
    
    
    /**
     * converte matriz de inteiros para matriz de floats
     * @param m matriz de inteiros
     * @param o ordem da matriz
     * @return 
     */
    public static double[][] matrizToDouble(int[][] m, int o){
        double[][] matriz = new double[o][o];
        for(int i = 0; i < o; i++){
            for(int j = 0; j < o; j++){
                matriz[i][j] = m[i][j];
            }
        }
        return matriz;
    } // end matrizToFloat
    

    /**
     * imprime uma matriz na consola
     * @param m matriz de floats
     * @param l nº de linhas
     * @param c nº de colunas
     */
    public static void imprimeMatrizDouble(double[][] m, int l, int c){
        for(int i = 0; i < l; i++){
            for(int j = 0; j < c; j++){
                System.out.printf("%10.3f", m[i][j]);
            }
            System.out.print("\n");
        }
    } // end imprimeMatriz
    
    
    /**
     * coloca zero na posição (x,y) da matriz fazendo operações elementares sobre
     * as linhas da matriz
     * @param m matriz
     * @param x índice da linha
     * @param y ínidce da coluna
     */
    public static void colocaZero(double[][] m, int x, int y){
        // é necessário guardar este valor, antes de ele ser alterado na matriz, pq vai ser usado outra para operar na matriz identidade
        double temp = m[y][y];
        // verificar se pivot == 0, caso seja troca p linha seguinte
        if(temp == 0){
            m = trocaLinha(m, y);
            temp = m[y][y];
            System.out.printf("Troca entre as linhas %d e %d\n", x, (x + 1));
        } // end if
        // se pivot != então procede com cálculos normais
        else{
            System.out.printf("Multiplica linha %d por %.2f\n", (x + 1), temp);
            // primeira operação elementar: multiplica linha em causa pelo valor da diagonal principal anterior
            multiplicaLinha(m,x,m[y][y]);
            // operação análoga sobre matriz identidade
            multiplicaLinha(ident,x,temp);
            // novamente é necessário guardar temp para depois ser usado na matriz identidade
            temp = m[x][y]/m[y][y];
            System.out.printf("Multiplica linha %d por %.2f\n", (y + 1), temp);
            // segunda operação elementar: multiplica linha do valor da diagonal principal usada pelo mod entre novo valor e valor da diagonal principal (só assim se consegue ter o mesmo valor para depois se subtrair)
            multiplicaLinha(m,y,(m[x][y]/m[y][y]));
            // operação análoga sobre matriz identidade
            multiplicaLinha(ident,y,temp);
            System.out.printf("Subtrai linha %d à linha %d\n\n", (x + 1), (y + 1));
            // terceira operação elementar: subtrai as linhas em causa para se ficar com zero na posição (x,y)
            subtraiLinhas(m,x,y);
            // operação análoga sobre matriz identidade
            subtraiLinhas(ident,x,y);
        } // end else
    } // end colocaZero
    
    
    /**
     * multiplica a linha de uma matriz m por um dado valor a
     * @param m matriz
     * @param l linha da matriz
     * @param a valor a multiplicar
     */
    public static void multiplicaLinha(double[][] m, int l, double a){
        for(int i = 0; i < m.length; i++){
            m[l][i] *= a;
        } // end for
    } // end multiplicaLinha
    
    
    /**
     * subtrai uma dada linha de uma matriz a outra
     * @param m matriz
     * @param l1 linha 1
     * @param l2 linha 2
     */
    public static void subtraiLinhas(double[][] m, int l1, int l2){
        for(int i = 0; i < m.length; i++){
            m[l1][i] -= m[l2][i];
        }
    } // end subtraiLinhas
    
    
    /**
     * reduz a diagonal principal da matriz à unidade
     * @param m matriz
     */
    public static void reduzirDiagonal(double[][] m){
        for(int i = 0; i < m.length; i++){
            dividirIdent(i, m[i][i]);
            m[i][i] /= m[i][i];
            // chama método para dividir matriz identidade pelo mesmo valor   
        }
    } // end procurarMinimoDiagonal
    
    
    /**
     * divide a linha l da matriz por valor f
     * @param l linha
     * @param f valor
     */
    public static void dividirIdent(int l, double f){
        for(int i = 0; i < ident.length; i++)
            ident[l][i] /= f; 
    } // end dividirIdent
    
    
    /**
     * copia o conteúdo da matriz m para inversa
     * @param m matriz
     */
    public static void guardarInversa(double[][] m){
        for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++){
                inversa[i][j] = m[i][j];
            }
        }
    } // end guardar inversa
    
    
    /**
     * volta a colocar a matriz identidade com os seus valores originais
     */
    public static void resetIdent(){
       for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++){
                if(i == j)
                    ident[i][j] = 1;
                else
                    ident[i][j] = 0;
            }
        }
    } // end resetIdent
    
    
    /**
     * imprime duas matrizes, a matriz a inverter e a matriz identidade
     * @param m1 matriz a inverter
     * @param m2 matriz identidade
     * @param l linhas
     * @param c colunas
     */
    public static void imprimeMatrizMetCond(double[][] m1, double[][] m2, int l, int c){
        for(int i = 0; i < l; i++){
            for(int j = 0; j < c; j++){
                System.out.printf("%15.3f", m1[i][j]);
            }
            System.out.printf("%5s", "|");
            for(int j = 0; j < c; j++){
                System.out.printf("%15.3f", m2[i][j]);
            }
            System.out.print("\n");
        }
    } // end imprimeMatriz
    
    
    /**
     * faz a troca entre as linhas l e l+1 da matriz m; faz também a mesma troca
     * na matriz identidade
     * @param m matriz
     * @param l linha
     * @return 
     */
    public static double[][] trocaLinha(double[][] m, int l){
        // array temp para guardar linha
        double[] temp = new double[m.length];
        double[] tempIdent = new double[m.length];    
        // copiar linha l para temp
        for(int i = 0; i < temp.length; i++){
            temp[i] = m[l][i];
        }
        // copiar linha l para temp (na matriz identidade)
        for(int i = 0; i < temp.length; i++){
            tempIdent[i] = ident[l][i];
        }   
        // copiar linha l+1 para l
        for(int i = 0; i < m.length; i++){
            m[l][i] = m[l + 1][i];
        }
        // copiar linha l+1 para l (na matriz identidade)
        for(int i = 0; i < m.length; i++){
            ident[l][i] = ident[l + 1][i];
        }    
        // copiar conteúdo de temp para l + 1
        for(int i = 0; i < m.length; i++){
            m[l + 1][i] = temp[i];
        }
        // copiar conteúdo de temp para l + 1 (na matriz identidade)
        for(int i = 0; i < m.length; i++){
            ident[l + 1][i] = tempIdent[i];
        }
        // retorna matriz m, já com as linhas l e l+1 trocadas entre si
        return m;
    } // end trocaLinha
    
    
    /**
     * descodifica mensagem
     */
    public static void descodificar(){
        // faz "reset" à variável de classe que guarda a mensagem descodificada
        msgDescod = "";
        // retirar espaços em branco no fim de msgCodificada
        msgCodificada.trim();
        // fazer split por espaços
        String[] temp = msgCodificada.split(" ");
        // variável contadora para controlar posição do array temp
        int cont = 0;
        // matriz de doubles para guardar conteudo de msgCodificada
        double[][] msgMatriz = new double[ordem][temp.length/ordem];
        // preencher msgMatriz
        for(int i = 0; i < msgMatriz.length; i++){
            for(int j = 0; j < msgMatriz[0].length; j++){
                msgMatriz[i][j] = Double.parseDouble(temp[cont]);
                cont++;
            } // end for
        } // end for
        // array que vai receber produto da matriz inversar * msgMatriz; o seu nº de linhas = ordem e nº colunas = colunas da msgMatriz
        double[][] produto = new double[ordem][temp.length/ordem];
        // efectua o produto entre matriz inversa e msgMatriz de forma a ter-se a mensagem original na forma numerica
        produto = multiplicaMatrizesDouble(inversa,msgMatriz,ordem,temp.length/ordem);
        //imprimeMatrizDouble(produto, produto.length, produto[0].length);
        // mostrar texto na consola
        int codigo = 0;
        for(int i = 0; i < produto.length; i++){
            for(int j = 0; j < produto[0].length; j++){
                // percorrer array de codigos
                for(int k = 0; k < tabelaConvCodigo.length; k++){
                    if(tabelaConvCodigo[k] == produto[i][j])
                        codigo = k;
                } // end for
                // vai construindo mensagem descodificada à medida que encontra caracteres na tabela conversora
                msgDescod += tabelaConvChars[codigo];
            } // end for
        } // end for
    } // end descodificar
    
    
    /**
     * imprime mensagem descodificada no ecrã
     */
    public static void mostrarTextoConsola(){
        // imprime mensagem descodificada no ecrã
        System.out.println(msgDescod);
    } // end mostrarTextoConsola
    
    
    /**
     * escreve mensagem descodificada em ficheiro de txt com nome a indicar pelo
     * utilizador
     * @throws FileNotFoundException 
     */
    public static void escreveMsgTxt() throws FileNotFoundException{
        boolean bl = false;
        String nomeFich = "";
        while(!bl){
            // pede nome do ficheiro ao utilizador
            System.out.println("Indique o nome a dar ao ficheiro que vai guardar a mensagem: ");
            String str = ler.nextLine();
            nomeFich = str + ".txt";
            File f = new File(nomeFich);
            // verificar se ficheiro existe
            if(f.exists()){
                System.out.println("O ficheiro indicado já existe!\n Pretende escrever por cima ou indicar outro nome?\n[E] Escrever por cima\n[O] Outro nome");
                String eO = ler.nextLine().toLowerCase();
                char op = eO.charAt(0);
                while(op != 'e' && op != 'o'){
                    System.out.println("Opção inválida! Tente novamente");
                    eO = ler.nextLine().toLowerCase();
                    op = eO.charAt(0);
                } // end while
                if(op == 'e'){
                    // sai do while
                    bl = true;
                }
            } // end if
            else
                bl = true;
        } // end while
        // obtenção de data do sistema
        Date actual = new Date();
        DateFormat formato = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String dataActual = formato.format(actual);
        // declaração de objecto do tipo Formatter
        Formatter fout = new Formatter(new File(nomeFich));
        fout.format("MENSAGEM DESCODIFICADA:\n" );
        fout.format(msgDescod);
        fout.format("\n\nData de criação: " + dataActual);
        fout.close();
        System.out.println("A mensagem foi guardada no ficheiro " + nomeFich);
    } // end escreveMsgTxt
    
    
    /**
     * calcula o produto entre duas matrizes m1 e m2
     * @param m1 matriz
     * @param m2 matriz
     * @param l número de linhas
     * @param c números de colunas
     * @return matriz resultante do produto de m1 por m2
     */
    public static double[][] multiplicaMatrizesDouble(double[][] m1, double[][] m2, int l, int c){
        double[][] produto = new double[l][c];
        // percorrer todas as posições da matriz produto
        for(int i = 0; i < l; i++){
            for(int j = 0; j < c; j++){
                double[] temp1 = new double[l];
                double[] temp2 = new double[l];
                // colocar valores em temp1 e temp2
                for(int a = 0; a < l; a++){
                    temp1[a] = m1[i][a];
                    temp2[a] = m2[a][j];
                } // end for
                // calcular produto na posição [i][j] da matriz produto
                produto[i][j] = somatorioDoubles(temp1, temp2, l);
            } // end for
        } // end for
        return produto;
    } // end multiplicaMatrizes
    
    
    /**
     * calcula o somatorio do produto dos elementos de dois arrays A e B, de i=0 ate
     * ao comprimento máximo do array
     * @param linha array A
     * @param coluna array B
     * @param l total de elementos a percorrer
     * @return somatorio do produto dos elementos
     */
    public static double somatorioDoubles(double[] linha, double[] coluna, int l){
        int somatorio = 0;
        for(int i = 0; i < l; i ++){
            somatorio += linha[i] * coluna[i];
        } // end for
        return somatorio;
    } // end somatorio
    
    
    /**
     * calcula matriz inversa pela matriz adjunta
     * @param html true para gerar html, false para mostrar na consoal
     */
    public static void matrizInversaAdj(){
        // declarar matriz de ints e ajustá-la a uma dimensão correcta
        int[][] matriz = matrizCorrectSize(matrizCod, ordem);  
        System.out.println("-> Passos detalhados de resolução através da matriz adjunta\n");
        System.out.println("\n-> Passo número 1: apresentação da fórmula geral\n");
        System.out.println("Inv = (1 / |M|) * Adj(M)");
        System.out.println("\nMatriz Adjunta (Adj) = ");
        imprimeAdjuntaGenerica(adjuntaGenerica,ordem);
        imprimeComponentesAdjunta(matrizCod);
        // array para guardar valores dos vários determinates (C11, C12, ..., Cnn)
        int[] determinantes = new int[(int)Math.pow(ordem,2)];
        // variável contadora para controlar posição no array determinantes
        int cont = 0;
        // declaração de matriz temporária para guardar as várias matrizes sobre as quais vai ser calculado o determinante
        int[][] temp = new int[ordem - 1][ordem - 1];
        // percorrer toda a matriz de forma a criar as várias matrizes
        for(int a = 0; a < matriz.length; a++){
            for(int b = 0; b < matriz.length; b++){
                // cria matriz com base na linha e coluna a omitir
                temp = criarMatrizFiltro(matriz, ordem, a, b);
                // é necessário diminuir ordem em 1 unidade já que as matrizes temp têm menos 1 linha e menos 1 coluna            
                determinantes[cont] = calculaDeterminante(temp, ordem - 1);
                // multiplica por -1 se soma dos indices for ímpar
                if(((a + 1) + (b + 1)) % 2 != 0)
                    determinantes[cont] *= -1;
                cont++;
            } // end for
        } // end for
        System.out.println("\n-> Passo número 2: apresentação do resultado dos determinantes\n");
        int det = calculaDeterminante(matriz,ordem); 
        System.out.println("|M| = " + det);
        int c1 = 1, c2 = 1;
        for(int i = 0; i < determinantes.length; i++){
            System.out.println("\n|C" + c1 + c2 + "| = " + determinantes[i]);
            c2++;
            if(c1 == ordem + 1)
                c1 = 1;
            if(c2 == ordem + 1){
                c2 = 1;
                c1++;
            } // end if  
        } // end for
        System.out.println("\n-> Passo número 3: apresentação da matriz adjunta\n");
        // determinar adjunta
        int[][] adj = new int[ordem][ordem];
        int c = 0;
        for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++){
                // esta troca entre j e i permite efectuar logo a transposição da matriz
                adj[j][i] = determinantes[c];
                c++;
            }
        }
        imprimeMatriz(adj,ordem,ordem);
        System.out.println("\n-> Passo número 4: apresentação da matriz inversa\n");
        // neste ponto é necessário ter-se dados do tipo double já que a inversa irá originar, muitas vezes, números com virgula flutuante
        double[][] adjDouble = matrizToDouble(adj,ordem);
        // calcula o factor que vai ser usado para multiplicar a matriz
        double invM = ((double) 1) / det;
        // chama método para multiplicar a matriz
        multiplicaMatrizK(adjDouble, invM);
        // apresenta resultado final
        imprimeMatrizDouble(adjDouble, ordem, ordem);
        // guarda matriz em variável global
        guardarInversa(adjDouble);
        // torna possível exportar para html
        existeInv = true;
    } // end matrizInversaAdj
    
    
    /**
     * imprime matriz adjunta (representação genérica)
     * @param m matriz adjunta (genérica)
     * @param o ordem
     */
    public static void imprimeAdjuntaGenerica(String[][] m, int o){
        for(int i = 0; i < o; i++){
            for(int j = 0; j < o; j++){
                System.out.printf("%10s", m[i][j]);
            }
            System.out.print("\n");
        }
    } // end imprimeAdjuntaGenérica
    
    
    /**
     * imprime os vários componentes da matriz adjunta
     * @param m matriz
     */
    public static void imprimeComponentesAdjunta(int[][] m){
        for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++){
                System.out.println("\n" + adjuntaGenerica[i][j] + " = ");
                imprimeMatrizDoubleFiltro(m,i,j);
            }
        }
    } // end imprimeComponentesAdjunta
    
    
    /**
     * imprime matriz excepto a linha l e a coluna c
     * @param m matriz
     * @param l linha
     * @param c coluna
     */
    public static void imprimeMatrizDoubleFiltro(int[][] m, int l, int c){
        for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++){
                if(i != l && j != c)
                    System.out.printf("%10d", m[i][j]);
            }
            if(i != l)
                System.out.print("\n");
        }
    } // end imprimeMatriz
    
    
    /**
     * cria uma matriz a partir de uma matriz m, filtrando a linha l e coluna c
     * @param m matriz 
     * @param ordem ordem da matriz
     * @param l linha a filtrar
     * @param c coluna a filtrar
     * @return 
     */
    public static int[][] criarMatrizFiltro(int[][] m, int ordem, int l, int c){
        int[][] temp = new int[ordem - 1][ordem - 1];
        for(int i = 0, x = 0; i < m.length; i++){
            for(int j = 0, y = 0; j < m.length; j++){
                if(i != l && j != c){
                    temp[x][y] = matrizCod[i][j];
                    y++;
                } // end if
            } // end for
            if(i != l)
                x++;
        } // end for
        return temp;
    }
    
   
    /**
     * cria uma matriz com dimensão adequada 
     * @param m matriz
     * @param o ordem da matriz
     * @return 
     */
    public static int[][] matrizCorrectSize(int[][] m, int o){
        int[][] matriz = new int[o][o];
        for(int i = 0; i < o; i++){
            for(int j = 0; j < o; j++){
                matriz[i][j] = matrizCod[i][j];
            }
        }
        return matriz;
    } // end matrizToFloat
    
    
    /**
     * calcula o determinante de uma matriz m de ordem o
     * @param m matriz
     * @param o ordem da matriz
     * @return 
     */
    public static int calculaDeterminante(int[][] m, int o){
        // declaração de variável para calcular determinante
        int det = 0;
        // define método usado para calcular determinante, com base na ordem da matriz
        if(o == 2){
            // chama regraPratica() se ordem == 2
            det = Emissor.regraPratica(m);
        }
        else if(o == 3){
            // chama sarrus() se ordem == 3
            det = Emissor.sarrus(m);
        }     
        else if(o == 4){
            // chama laPlace() se ordem == 4
            det = laPlace(m);
        }
        /*
        else
            // como a aplicação só admite matrizes de ordem 2, 3 ou 4, caso seja diferente é por que não existe nenhuma matriz no sistema
            System.out.print("Não existe nenhuma matriz carregada na aplicação!");
        */
        //retorna determinante
        return det;
    } // end calculaDerminante
    
    
    /**
     * imprime matriz de inteiros no ecrã
     * @param m matriz
     * @param l número de linhas
     * @param c número de colunas
     */
    public static void imprimeMatriz(int[][] m, int l, int c){
        for(int i = 0; i < l; i++){
            //System.out.println("----------------------------");
            for(int j = 0; j < c; j++){
                System.out.printf("%7d", m[i][j]);
            }
            System.out.print("\n");
        }
        //System.out.println("----------------------------");
    } // end imprimeMatriz
    
    
    /**
     * multiplica todos os elementos de uma matriz m por um factor k
     * @param m matriz
     * @param k factor usado para multiplicar matriz
     */
    public static void multiplicaMatrizK(double[][] m, double k){
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m.length; j++){
                m[i][j] *= k;
            }
        }
    } // end multiplicaMatrizK
    
    
    /**
     * gera documento html com matriz inversa
     * @throws FileNotFoundException 
     */
    public static void matrizInversaAdjHtml() throws FileNotFoundException {
        if(!existeInv)
            System.out.println("Não é possível gerar documento html! \nAinda não foi calculada nenhuma matriz inversa!");
        else{
            CodificadorHTML.codificarHTML("matrizInvAdj", "Calculo da Matriz Inversa (pela matriz adjunta)");
            System.out.println("Ficheiro html gerado com sucesso!");
        }
    } // end matrizInversaAdjHtml
    
    
    public static void matrizInversaIdentHtml() throws FileNotFoundException {
        if(!existeInv)
            System.out.println("Não é possível gerar documento html! \nAinda não foi calculada nenhuma matriz inversa!");
        else{
            CodificadorHTML.codificarHTML("matrizInvIdent", "Calculo da Matriz Inversa (pela matriz identidade)");
            System.out.println("Ficheiro html gerado com sucesso!");
        }
    } // end matrizInversaAdjHtml
    
    
    /**
     * devolve matriz codificante
     * @return 
     */
    public static int[][] getMatrizCod(){
        return matrizCod;
    } // end getMatrizCod
    
    /**
     * devolve matriz inversa
     * @return 
     */
    public static double[][] getInversa(){
        return inversa;
    } // end getInversa
    
    
    /**
     * devolve ordem da matriz
     * @return 
     */
    public static int getOrdem(){
        return ordem;
    } // end getOrdem
    
    
    /**
     * método de LaPlace para calcular determinante de uma matriz 4*4; baseia-se
     * na geração de várias matrizes 3*3 e do recurso ao métodos de Sarrus para
     * calcular o determinante de cada uma delas
     * @return 
     */
    public static int laPlace(int[][] m){
        // declarar variável para guardar valor do determinante (inicializada a zero para se poder somar nas várias iteracções do ciclo)
        int det = 0;
        // declarar matriz temporaria 3*3 para usar em sarrus()
        int[][] temp = new int[3][3];
        // chamar sarrus() num total de 4x
        for(int a = 0; a < 4; a++){
            // preencher matriz temporária
            preencherMatrizTemp(temp,a);
            // calcula determinante para cada matriz temp e acumula em det (teorema de LaPlace)
            det += m[3][a] * Math.pow(-1, 4 + (a+1)) * Emissor.sarrus(temp);
        }
        // devolve determinante da matriz 4*4
        return det;
    } // end laPlace
    
    
    /**
     * método para preencher matriz temporária 3*3 a partir de uma matriz 4*4,
     * necessário para laPlace
     * @param m matriz temporaria 3*3
     * @param a indica coluna a não ser copiada para a matriz
     */
    public static void preencherMatrizTemp(int[][] m, int a){
        /* ignorar linha 4 é alcançado com o for a correr até apenas <3, isto é,
         * assim só percorre as linhas 0, 1 e 2 da matriz 4*4:
         * no método de laPlace é necessário ignorar uma linha e
         * neste caso optou-se por ignorar sempre a última linha; são necessárias
         * duas variaveis: i (para controlar linha da matriz 4*4) e k (para controlar
         * linha da matriz 3*3) */ 
        for(int i = 0, k = 0; i < 3; i++, k++){ // tanto i como k podem ser incrementados no ciclo exterior já que a linha a eliminar é sempre a mesma
            for(int j = 0, l = 0; j < 4; j++){ // apenas a coluna da matriz 4*4 é incrementada
                // quando a coluna é igual à que é passada por parâmetro não se copiam os valores
                if(j != a){
                    // quando a coluna é diferente da q é passada por parâmetro pode-se copiar os valores
                    m[k][l] = matrizCod[i][j];
                    // apenas se pode incrementar a coluna da matriz 3*3 quando existe atribuição do valor da 4*4 para esta matriz
                    l++;
                } // end if
            } // end for
        } // end for
    } // end preencherMatrizTemp
    
    
} // end class Receptor
    
    
  