package lapr1.nbproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Classe que contem todos os metodos para extrair os varios dados a partir de 
 * um ficheiro em formato html
 * @author João Carreira
 */
public class DescodificadorHTML {
    
    // variável de classe para guarda mensagem codificada na forma de string
    private static String msgCodificada = "";
    
    
    /**
     * lê mensagem codificada a partir do ficheiro mensagemCodificada.html
     * @param nomeFich nome do ficheiro onde está a mensagem codificada
     * @throws FileNotFoundException 
     */
    public static void lerMsgCodificada(String nomeFich) throws FileNotFoundException{
        // objecto do tipo Scanner para ler ficheiro
        Scanner fin = new Scanner(new File(nomeFich));
        while(fin.hasNextLine()){
            String temp = fin.nextLine();
            // quando encontra <p> corresponde à mensagem codificada embutida no html
            if(temp.startsWith("\t<p align=\"center\">")){
                // faz split por espaços
                String[] vecNum = temp.split(" ");
                // novo vector para guardar códigos dos caracteres
                String[] vecNumFinal = new String[vecNum.length];
                // ciclo for que percorre vecNum
                for(int i = 1; i < vecNum.length; i++){
                    // no vecFinal guarda cada posição de vecNum em que tudo o que é alfabetico foi retirado
                    vecNumFinal[i] = vecNum[i].replaceAll("\\D", "");
                    // todos os códigos resultantes são guardados em msgCodificada
                    msgCodificada += vecNumFinal[i] + " ";
                } // end for
            } // end if
        } // end while 
        fin.close();
    } // end lerMsgCodificada
    
    
    /**
     * lê matriz codificadora a partir do ficheiro matrizCodificadora.html
     * @param nomeFich nome do ficheiro html
     * @throws FileNotFoundException 
     */
    public static void lerMatriz(String nomeFich) throws FileNotFoundException{
        Scanner fin = new Scanner(new File(nomeFich));
        // variável para guardar a ordem da matriz
        int ordem = 0;
        String matriz = "";
        // leitura do ficheiro html
        while(fin.hasNextLine()){
            String temp = fin.nextLine();
            // sempre que passa por esta linha significa que se tem uma nova linha, logo a ordem da matriz é incrementada
            if(temp.startsWith("\t\t\t\t\t\t<tr>")){
                ordem++;
            }
            // sempre que passa por esta linha vai buscar o respectivo valor
            if(temp.startsWith("\t\t\t\t\t\t\t<td align=\"center\" valign=\"middle\" width=60>")){
                // remove todos os não digitos
                String temp2 = temp.replaceAll("\\D","");
                // remove os digitos respeitantes as propriedades da célula da tabela html
                temp2 = temp2.substring(2, temp2.length());
                // coloca tudo na string matriz
                matriz += temp2 + " ";
            }
        } // end while
        // chama método da classe Receptor que vai permitir guardar dados da matriz na repectiva variável de classe
        Receptor.importMatriz(matriz, ordem);
        fin.close();
    } // end lerMatriz
    
    
    /**
     * lê a tabela conversora a partir do ficheiro tabelaConversora.html
     * @param nomeFich
     * @throws FileNotFoundException 
     */
    public static void lerTabelaConv(String nomeFich) throws FileNotFoundException{
        // objecto do tipo Scanner para ler ficheiro html
        Scanner fin = new Scanner(new File(nomeFich));
        // variavel para guardar os caracteres da tabela
        String caracteres = "";
        // variavel para guardar os códigos da tabela
        String codigos = "";
        int cont = 0;
        while(fin.hasNextLine()){
            String temp = fin.nextLine();
            // quando lê esta linha encontra uma table row que ou é de carateres ou de códigos
            if(temp.startsWith("\t\t<tr>"))
                // incrementa para mais à frente se saber se é de caracteres ou de códigos
                cont++;
            // condição que indica que a table data é de caracteres
            if(temp.startsWith("\t\t\t<td align=\"center\" valign=\"middle\" width=30>") && cont == 1)
                    caracteres += temp.charAt(47) + " ";
            // condição que indica que table data é de códigos
            if(temp.startsWith("\t\t\t<td align=\"center\" valign=\"middle\" width=30>") && cont == 2){
                    String temp2 = temp.replaceAll("\\D","");
                    temp2 = temp2.substring(2, temp2.length());
                    codigos += temp2 + " ";
            }
        } // end while
        Receptor.importTabelaConv(caracteres, codigos);
    } // end lerTabelConv
    
    
    /**
     * retorna string que contém a mensagem codificada
     * @return 
     */
    public static String getMsgCodificada(){
        return msgCodificada;
    } // end getMsgCodificada
    
} // end DescodificadorHTML
