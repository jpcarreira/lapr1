package lapr1.nbproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;


/**
 * Classe que tem todos os métodos responsáveis pela codificação, em HTML, das várias
 * funcionalidades pedidas pela classe emissor; é construído em torno de métodos estáticos
 * que retornam strings e tabelas formatadas em HTML
 * @author João Carreira
 */
public class CodificadorHTML {
    
    public static void codificarHTML(String nomeFich, String titulo) throws FileNotFoundException{
        Formatter fout = new Formatter(new File(nomeFich+".html"));
        corpoInicioHTML(fout, nomeFich, titulo);
        if(nomeFich == "matrizCodificadora")
            divMatrizCodificadora(fout);
        else if(nomeFich == "tabelaConversora")
            divTabelaConversora(fout);
        else if(nomeFich == "mensagemCodificada")
            divMsgCodificada(fout);
        else if(nomeFich == "matrizInvAdj")
            divMatrizInv(fout);
        else if(nomeFich == "matrizInvIdent")
            divMatrizInv(fout);
        footerHTML(fout);
    }
    
    
    /**
     * método que escreve o corpo de início do ficheiro HTML; pára antes da
     * primeira <div>
     * @param fout
     * @param nomeFich
     * @param titulo
     * @throws FileNotFoundException 
     */
    public static void corpoInicioHTML(Formatter fout, String nomeFich, String titulo) throws FileNotFoundException{
        fout.format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        fout.format("%n<html>");
        fout.format("%n<head>");
        fout.format("%n\t<title>"+titulo+"</title>");
        fout.format("%n\t<style type=\"text/css\">");
        fout.format("%n\t\th2{text-align: center; color: #0000FF; font-size: 30px;}");
        fout.format("%n\t\th3{text-align: center;}");
        fout.format("%n\t\t#one{position: relative; top: 10px;}");
        fout.format("%n\t\t#two{position: absolute; bottom: 150px; right: 200px; text-align: center; color: #000; font-size: 12px;}");
        fout.format("%n\t</style>");
        fout.format("%n</head>");
        fout.format("%n<body>");
        fout.format("%n<h2>" + titulo + "</h2>");
    } // end corpoInicioHTML
    
    
    /**
     * método que acrescenta a segunda <div> ao ficheiro HTML
     * @param fout objecto do tipo Formatter
     */
    public static void footerHTML(Formatter fout){
        // obter data actual e formatá-la 
        Date actual = new Date();
        DateFormat formato = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String dataActual = formato.format(actual);
        
        fout.format("%n<div id=\"two\">");
        fout.format("%n\t<p><em>Data/hora: " + dataActual + "</em></p>");
        fout.format("%n\t<p><em>LAPR1 - Grupo DMN13</em></p>");
        fout.format("%n\t<p><em>Joao Carreira & Paulo Ponciano</em></p>");
        fout.format("%n</div>");
        fout.format("%n</body>");
        fout.format("%n</html>");
        fout.close();
    } // end footerHTML
    
    
    /**
     * método para codificar para HTML a <div> que diz respeito ao ficheiro HTML
     * da matriz codificadora
     * @param fout objecto do tipo Formatter 
     */
    public static void divMatrizCodificadora(Formatter fout){
        fout.format("%n<div id=\"one\">");
        fout.format("%n\t<table border=0 cellpadding=0 cellspacing=0px align=center style=\"border-left:1px solid #000; border-right:1px solid #000; color:#000;\">");
        fout.format("%n\t\t<tr>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t\t\t<td>");
        fout.format("%n\t\t\t\t\t<table border=0 cellpadding=0 cellspacing=0 style=\"color:#000;\">");
        
        // vai buscar a matriz coficante e guarda em temp
        int[][] temp = Emissor.getMatrizCod();
        // conta quantas linhas ou colunas tem
        int cont = Emissor.matrizValida();
        
        // inicia ciclo de preenchimento da matriz
        // percorre linhas
        for(int i = 0; i < cont; i++){
            fout.format("%n\t\t\t\t\t\t<tr>");
            // percorre colunas
            for(int j = 0; j < cont; j++){
                // adiciona ao ficheiro HTML o valor da posição [i][j]
                fout.format("%n\t\t\t\t\t\t\t<td align=\"center\" valign=\"middle\" width=60>%d</td>", temp[i][j]);
            } // end for
            fout.format("%n\t\t\t\t\t\t</tr>");
        } // end for
        
        fout.format("%n\t\t\t\t\t</table>");
        fout.format("%n\t\t\t\t</td>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t</tr>");
        fout.format("%n\t\t</table>");
        fout.format("%n</div>");
    } // end divMatrizCodificadora
    
    
    /**
     * codifica, para ficheiro HTML, a <div> que diz resppeito ao ficheiro HTML
     * da tabela conversora
     * @param fout objecto do tipo Formatter
     */
    public static void divTabelaConversora(Formatter fout){
        fout.format("%n<div id=\"one\">");
        fout.format("%n\t<table border=1 cellpadding=0 cellspacing=0px align=center style=\"border:1px solid #000 color:#000;\">");
        
        // vai buscar arrays com dados da tabela conversora
        char[] tempChar = Emissor.getTabelaConvChars();
        int[] tempInt = Emissor.getTabelaConvCodigo();
        int cont = Emissor.tabelaValida();
        
        // ínicia ciclo de preenchimento da tabela
        fout.format("%n\t\t<tr>");
        // percorre a tabela de caracteres
        for(int i = 0; i < cont; i++){
            fout.format("%n\t\t\t<td align=\"center\" valign=\"middle\" width=30>%c</td>",tempChar[i]);
        } // end for
        fout.format("%n\t\t</tr>");
        fout.format("%n\t\t<tr>");
        // percorre a tabela de códigos
        for(int i = 0; i < cont; i++){
            fout.format("%n\t\t\t<td align=\"center\" valign=\"middle\" width=30>%d</td>",tempInt[i]);
        } // end for
        fout.format("%n\t\t</tr>");
        fout.format("%n\t</table>");
        fout.format("%n</div>");
    } // end divTabelaConversora
    
    
    /**
     * codifica, para ficheiro HTML, a <div> que diz respeito ao ficheiro HTML com a 
     * mensagem codificada
     * @param fout objecto do tipo Formatter
     * @throws FileNotFoundException 
     */
    public static void divMsgCodificada(Formatter fout) throws FileNotFoundException{
        // abre ficheiro de texto com a mensagem codificada e guardar todos esses valores em msg
        Scanner fin = new Scanner(new File("mensagemCodificada.txt"));
        String temp = fin.nextLine();
        String[] msg = temp.split(" ");
        fout.format("%n<div id=\"one\">");
        fout.format("%n\t<p align=\"center\">");
        // ciclo para escrever conteúdo de msg
        for(int i = 0; i < msg.length; i++){
            fout.format(msg[i] + " ");
        }
        fout.format("</p>");
        fout.format("%n</div>");
    } // end msgCodificada
    
    
    /**
     * codifica, para ficheiro HTML, a <div> que diz respeito a matriz inversa
     * @param fout objecto do tipo Formatter
     * @throws FileNotFoundException 
     */
    public static void divMatrizInv(Formatter fout) throws FileNotFoundException{
        fout.format("%n<div id=\"one\">");
        fout.format("<h3>Matriz Codificante</h3>");
        fout.format("%n\t<table border=0 cellpadding=0 cellspacing=0px align=center style=\"border-left:1px solid #000; border-right:1px solid #000; color:#000;\">");
        fout.format("%n\t\t<tr>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t\t\t<td>");
        fout.format("%n\t\t\t\t\t<table border=0 cellpadding=0 cellspacing=0 style=\"color:#000;\">");
        // vai buscar a matriz coficante e guarda em temp
        int[][] temp = Receptor.getMatrizCod();
        // conta quantas linhas ou colunas tem
        int cont = Receptor.getOrdem();
        // inicia ciclo de preenchimento da matriz
        // percorre linhas
        for(int i = 0; i < cont; i++){
            fout.format("%n\t\t\t\t\t\t<tr>");
            // percorre colunas
            for(int j = 0; j < cont; j++){
                // adiciona ao ficheiro HTML o valor da posição [i][j]
                fout.format("%n\t\t\t\t\t\t\t<td align=\"center\" valign=\"middle\" width=100>%d</td>", temp[i][j]);
            } // end for
            fout.format("%n\t\t\t\t\t\t</tr>");
        } // end for
        fout.format("%n\t\t\t\t\t</table>");
        fout.format("%n\t\t\t\t</td>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t</tr>");
        fout.format("%n\t\t</table>");
        
        fout.format("<h3>Matriz Inversa</h3>");
        fout.format("%n\t<table border=0 cellpadding=0 cellspacing=0px align=center style=\"border-left:1px solid #000; border-right:1px solid #000; color:#000;\">");
        fout.format("%n\t\t<tr>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t\t\t<td>");
        fout.format("%n\t\t\t\t\t<table border=0 cellpadding=0 cellspacing=0 style=\"color:#000;\">");   
        // vai buscar a matriz coficante e guarda em temp
        double[][] temp2 = Receptor.getInversa();
        // conta quantas linhas ou colunas tem
        int cont2 = Receptor.getOrdem();    
        // inicia ciclo de preenchimento da matriz
        // percorre linhas
        for(int i = 0; i < cont2; i++){
            fout.format("%n\t\t\t\t\t\t<tr>");
            // percorre colunas
            for(int j = 0; j < cont2; j++){
                // adiciona ao ficheiro HTML o valor da posição [i][j]
                fout.format("%n\t\t\t\t\t\t\t<td align=\"center\" valign=\"middle\" width=100>%f</td>", temp2[i][j]);
            } // end for
            fout.format("%n\t\t\t\t\t\t</tr>");
        } // end for     
        fout.format("%n\t\t\t\t\t</table>");
        fout.format("%n\t\t\t\t</td>");
        fout.format("%n\t\t\t<td style =\"border-top:1px solid #000; border-bottom:1px solid #000;\">&nbsp</td>");
        fout.format("%n\t\t</tr>");
        fout.format("%n\t\t</table>");
        fout.format("%n</div>");
    } // end divMatrizInvAdj
    
} // end CodificadorHTML
