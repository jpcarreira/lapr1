package lapr1.nbproject;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


/**
 * Classe Emissor: faz a encriptação da meensagem
 * @author axpppp
 */

public class Emissor {
    
    /*
     * variáveiss globais
     */
    // objecto ler to do tipo Scanner como static (para poder ser usado em toda a classe)
    private static Scanner ler = new Scanner(System.in).useDelimiter("\n");
    // variável global para guardar mensagem a ser codificada
    private static String mensagem = "";
    // variável gloval para guardar nome do ficheiro de texto que contem mensagem a codificar
    private static String nomeFich = "";
    // variável global para matriz codificador, inicializada com todos os valores a -1 (útil para o imprimir matriz)
    private static int[][] matrizCod = new int[][]{{-1,-1,-1,-1}, {-1,-1,-1,-1}, {-1,-1,-1,-1}, {-1,-1,-1,-1}};
    // variável global para a ordem da matriz (iniciado a -1 para indicar que matriz não é válida)
    private static int ordem;
    // variável global que guarda nome do ficheiro csv
    private static String nomeFichCsv = "";
    // variável global que guardar os caracteres da tabela conversora (com capacidade para 200)
    private static char[] tabelaConvChars = new char[200];
    // variável global que guardar o código de caracteres da tabela conversora (com capidade para 200)
    private static int[] tabelaConvCodigo = new int[200];
    // variável booleana que controla se foi codificada mensagem
    private static boolean existeMsg = false;
    
    
    /**
     * Metodo Main, que invoca os Menus para escolher o que se quer fazer.  
     */
    public static void main(String[] args) throws Exception {
        welcome();
    } // end main

    
    /**
     * Menu de boas-vindas da aplicação emissora
     */
    public static void welcome() throws Exception {
        System.out.println("****************************************************");
        System.out.println("*                                                  *");
        System.out.println("* ISEP - INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO *");
        System.out.println("*                                                  *");
        System.out.println("*   Departamento de Engenharia Informática         *");
        System.out.println("*               LAPR1 - 2012-2013                  *");
        System.out.println("*                                                  *");
        System.out.println("*                                                  *");
        System.out.println("*          <-- Aplicação Emissora -->              *");
        System.out.println("*                                                  *");
        System.out.println("*                                                  *");
        System.out.println("*       Autores:                                   *");
        System.out.println("*           João Carreira                          *");
        System.out.println("*           Paulo Ponciano                         *");
        System.out.println("*                                                  *");															  
        System.out.println("*                                   Dezembro 2012  *");
        System.out.println("*                                                  *");
        System.out.println("****************************************************");
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*                  Correr programa (c) / Sair (s)  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            // guarda opção (converte para lowercase)
            String opcao = (ler.nextLine()).toLowerCase();
            // converte para char (considera apenas 1º caracter)
            char opcao2 = opcao.charAt(0);
            if(opcao2 == 'c'){
                menuEmissor();
                sair = true;
            }
            else if(opcao2 == 's'){
                System.out.println("Programa terminado!");
                sair = true;
            }
            else
                System.out.println("Opção inválida!");
        } // end while 
    } // end welcome
    
    
    /**
     * Menu da aplicação emissora
     */
    public static void menuEmissor() throws Exception {
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*               Aplicação Emissora                 *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     1 - Ler texto                                *");
            System.out.println("*     2 - Mostrar texto                            *");
            System.out.println("*     3 - Ler matriz codificadora                  *");
            System.out.println("*     4 - Gerar matriz codificadora                *");
            System.out.println("*     5 - Escrever matriz para HTML                *");
            System.out.println("*     6 - Ler tabela conversora                    *");
            System.out.println("*     7 - Escrever tabela conversora para HTML     *");
            System.out.println("*     8 - Mostrar mensagem codificada como matriz  *");
            System.out.println("*     9 - Escrever mensagem codificada para HTML   *");
            System.out.println("*                                                  *");
            System.out.println("*     S - Sair                                     *");															  
            System.out.println("*                                                  *");
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = ler.nextLine();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case '1':
                    lerTexto();
                    break;
                case '2':
                    imprimeTexto();
                    break;
                case '3':
                    lerMatrizCod();
                    break;
                case '4':
                    gerarMatriz();
                    break;
                case '5':
                    if(existeMatrizCod(ordem)){
                        CodificadorHTML.codificarHTML("matrizCodificadora","Matriz Codificadora");
                        System.out.println("Ficheiro HTML gerado com sucesso");
                    }
                    else
                        System.out.println("Não existe nenhuma matriz codificadora no sistema!");
                    break;
                case '6':
                    lerTabelaConv();
                    break;
                case '7':
                    if(existeTabelaConv(tabelaConvChars, tabelaConvCodigo)){
                        CodificadorHTML.codificarHTML("tabelaConversora", "Tabela Conversora");
                        System.out.println("Ficheiro HTML gerado com sucesso");
                    }
                    else
                        System.out.println("Não existe nenhuma tabela conversora no sistema!");
                    break;
                case '8':
                    mostrarMsgCodificada();
                    break;
                case '9':
                    if(existeMsg){
                        CodificadorHTML.codificarHTML("mensagemCodificada", "Mensagem Codificada");
                        System.out.println("A mensagem codificada foi escrita, com sucesso, num ficheiro HTML!");
                    }
                    else{
                        System.out.println("Esta opção só pode ser executada quando após executar com sucesso a opção 8.!!");
                    }
                    break;
                case 's':
                    System.out.println("Programa terminado!");
                    sair = true;
                    break;
                case 'S':
                    System.out.println("Programa terminado!");
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
            
        } // end while
    } // end menuEmissor
    
    
    /**
     * ler texto, a partir do teclado ou ficheiro de texto
     */
    public static void lerTexto() throws Exception {
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*               1. Ler texto                       *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     T - A partir do teclado                      *");
            System.out.println("*     F - A partir de um ficheiro de texto         *");
            System.out.println("*     V - Voltar ao menu anterior                  *");															  
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            // lê string e converte para lowercase para facilitar switch case
            String opcao = (ler.nextLine()).toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case 't':
                    // chama função para ler a partir do teclado
                    lerTextoTeclado();
                    // sair passa a true para voltar ao menu anterior
                    sair = true;
                    break;
                case 'f':
                    // chama função para ler a partir do ficheiro de texto
                    lerFicheiro();
                    // sair passa a true para voltar ao menu anterior
                    sair = true;
                    break;
                case 'v':
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
        } // end while
    } // end lerTexto()

    
    /**
     * ler matriz codificadora, a partir do teclado ou ficheiro csv
     */
    public static void lerMatrizCod() throws Exception {
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*               3. Ler matriz codificadora         *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     T - A partir do teclado                      *");
            System.out.println("*     F - A partir de um ficheiro csv              *");
            System.out.println("*     V - Voltar ao menu anterior                  *");															  
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = (ler.nextLine()).toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case 't':
                    // chama função para ler a partir do teclado
                    lerMatrizTeclado();
                    // sair passa a true para voltar ao menu anterior
                    sair = true;
                    break;
                case 'f':
                    // chama função para ler a partir de ficheiro csv
                    lerMatrizCsv();
                    // sair passa a true para voltar ao menu anterior
                    sair = true;
                    break;
                case 'v':
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
        } // end while
    } // end lerMatrizCod()
    
    
    /**
     * ler tabela conversora, a partir do teclado ou ficheiro csv
     */
    public static void lerTabelaConv() throws Exception {
        boolean sair = false;
        while(!sair){
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*               6. Ler tabela conversora           *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            System.out.println("*                                                  *");
            System.out.println("*     T - A partir do teclado                      *");
            System.out.println("*     F - A partir de um ficheiro csv              *");
            System.out.println("*     V - Voltar ao menu anterior                  *");															  
            System.out.println("*                                                  *");
            System.out.println("*--------------------------------------------------*");
            System.out.println("*                                                  *");
            System.out.println("*               Escolha uma opção                  *");
            System.out.println("*                                                  *");
            System.out.println("****************************************************");
            String opcao = (ler.nextLine()).toLowerCase();
            char opcao2 = opcao.charAt(0);
            switch(opcao2){
                case 't':
                    // chama função para ler tabela conversora de caracteres a partir do teclado
                    lerTabelaConvTeclado();
                    sair = true;
                    break;
                case 'f':
                    // chama função para ler tabela conversar a partir de ficheiro csv
                    lerTabelaConvCsv();
                    sair = true;
                    break;
                case 'v':
                    sair = true;
                    break;
                default:
                    System.out.println("Introduza uma opção válida!");
            } // end switch
        } // end while
    } // end lerTabelaConv()

    
    /**
     * método para ler texto a partir do teclado
     */
    public static void lerTextoTeclado(){
        System.out.println("Escreva a mensagem que pretende codificar:");
        // guarda texto na variável global
        mensagem = ler.nextLine();
        // avisa utilizador que não foi introduzido nenhum texto
        if(mensagem.equals(""))
            System.out.println("Não introduziu qualquer mensagem!\n");
        else
            System.out.println("Mensagem gravada com suscesso!");
    } // end lerTextoTeclado()
    
    
    /**
     * método que imprime mensagem a codificar na consola
     */
    public static void imprimeTexto(){
        if(!mensagem.equals(""))
            System.out.println("A mensagem que prentende codificar é: \n" + mensagem + "\n");
        else
            System.out.println("Não foi introduzida nenhuma mensagem!");
    } // end imprimeTexto() 
    
    
    /**
     * método para ler mensagem a partir de um ficheiro de texto
     */
    public static void lerFicheiro() throws Exception {
        // "limpa" mensagem original
        mensagem = "";
        // pede ao utilizador o nome do ficheiro
        System.out.println("Indique o nome do ficheiro que contém a mensagem, incluindo a extensão");
        System.out.println("(exemplo: mensagem.txt)");
        nomeFich = ler.nextLine();
        // verificar se foi introduzido nome para o ficheiro
        if(nomeFich.equals(""))
            System.out.println("Nome de ficheiro inválido!");
        // declaração da variável do tipo ficheiro
        File f = new File(nomeFich);
        // passo para verificar se ficheiro existe
        // se não existir informa o utilizador
        if(!f.exists())
            System.out.println("O ficheiro que indicou não existe!");
        // se ficheiro não tiver texto informa o utilizador
        else if(f.length() == 0)
            System.out.println("O ficheiro indicado não tem conteúdo!");
        // se existir faz leitura do ficheiro
        else{
            // declaração de objecto do tipo scanner para ler o ficheiro
            Scanner fin = new Scanner(f);
            // ler o ficheiro linha a linha
            while(fin.hasNextLine()){
                String temp = fin.nextLine();
                // faz concatenação com a string mensagem à medida que lê as várias linhas
                mensagem += temp /*+ "\n"*/;
            } // end while
            // fecha o ficheiro
            fin.close();
            // mensagem a informar que ficheiro foi lido com sucesso
            System.out.println("Ficheiro lido com sucesso!");
        } // end else
    } // end lerFicheiro()
    
    
    /**
     * método para ler matriz codificadora a partir do teclado
     */
    public static void lerMatrizTeclado() throws Exception { 
        // faz reset da matriz
        resetMatriz();
        // boolean como flag para verificar se matriz admite inversa
        boolean invertivel = false;
        pedirOrdemMatriz();
        while(!invertivel){
            // leitura da matriz
            int temp2;
            try{
                for(int i = 0; i < ordem; i++){
                    for(int j = 0; j < ordem; j++){
                        // pede número ao utilizador
                        System.out.printf("Introduza o valor da %dª linha, %dª coluna:\n", (i+1), (j+1));
                        temp2 = ler.nextInt();
                        // verifica se valor introduzido é número inteiro positivo
                        while(temp2 < 0){
                            System.out.printf("Valor inválido!\nIntroduza um valor inteiro positivo para a %dª linha, %dª coluna:\n", (i+1), (j+1));
                            temp2 = ler.nextInt();
                        }
                        matrizCod[i][j] = temp2;
                    } // end for
                } // end for
            } // end try
            // lança excepção
            catch(Exception exc){
                System.out.println("Operação cancelada! Tem que introduzir um valor numérico!");
            }
            // linha seguinte impede que o programa "crashe" ao usar objecto static ler
            ler.nextLine();
            // avisa utilizador caso a matriz não seja invertível
            if(calculaDeterminante(matrizCod) == 0)
                System.out.printf("A matriz introduzida não é invertível! \nPor favor introduza novos valores para a matriz %dx%d!\n", ordem, ordem);
            // se matriz fôr invertível coloca flag a true para permitir sair do ciclo while
            else
                invertivel = true;
        } // endwhile
        // apagar linha seguinte, apenas para teste
        imprimeMatriz(matrizCod, ordem, ordem);
    } // end lerMatrizTeclado
    
    
    /**
     * método para mostrar, no ecrã, uma dada matriz passada por parâmetro
     * @param m matriz bidimensional
     * @param l total de linhas
     * @param c total de colunas
     */
    public static void imprimeMatriz(int[][] m, int l, int c){
        System.out.println("\nMatriz:");
        for(int i = 0; i < l; i++){
            //System.out.println("----------------------------");
            for(int j = 0; j < c; j++){
                // só imprime quando é diferente do valor ao qual a matriz foi incializada
                if(m[i][j] != -1)
                    System.out.printf("%7d", m[i][j]);
                    //System.out.print("  |" + m[i][j] + "|");
            }
            System.out.print("\n");
        }
        //System.out.println("----------------------------");
    } // end imprimeMatriz
    
    
    /**
     * método para ler matriz codificadora através de ficheiro csv
     */
    public static void lerMatrizCsv() throws Exception {
        // faz reset da matriz
        resetMatriz();
        boolean leuMatriz = false;
        // "limpa" mensagem original ?????
        //mensagem = "";
        // pede ao utilizador o nome do ficheiro
        System.out.println("Indique o nome do ficheiro que contém a matriz codificadora, incluindo a extenção");
        System.out.println("(exemplo: matriz.csv)");
        nomeFichCsv = ler.nextLine();
        // verificar se foi introduzido nome para o ficheiro
        if(nomeFichCsv.equals(""))
            System.out.println("Nome de ficheiro inválido!");
        // declaração da variável do tipo ficheiro
        File f = new File(nomeFichCsv);
        // passo para verificar se ficheiro existe
        // se não existir informa o utilizador
        if(!f.exists())
            System.out.println("O ficheiro que indicou não existe!");
        // se ficheiro não tiver texto informa o utilizador
        else if(f.length() == 0)
            System.out.println("O ficheiro indicado não tem conteúdo!");
        // se existir faz leitura do ficheiro
        else{
            // declaração de objecto do tipo scanner para ler o ficheiro
            Scanner fin = new Scanner(f);
            // variável contadora para verificar em que linha se encontra
            int cont = 0;
            // ler o ficheiro linha a linha
            while(fin.hasNextLine()){
                String temp = fin.nextLine();
                // incrementa contador para actualizar linha em que se está
                cont++;
                // caso esteja na primeira linha
                if(cont == 1)
                    ordem = Integer.parseInt(temp);
                // caso não seja a primeira linha faz a leitura dos valores
                else{
                    // vector para guardar valores do ficheiro csv
                    String[] valores = temp.split(";");
                    // ordem ao quadrado tem que ser igual ao total de valore
                    if(Math.pow(ordem, 2) == valores.length){
                        // variável contadora para percorrer array anterior
                        int cont2 = 0;
                        // preencher matriz
                        for(int i = 0; i < ordem; i++){
                            for(int j = 0; j < ordem; j++){
                                // introduz o n-ésimo valor do array valores na matriz
                                matrizCod[i][j] = Integer.parseInt(valores[cont2]);
                                // incrementa para na interacção seguinte passar ao n-ésimo + 1 do array valores
                                cont2++;
                            } // end for
                        } // end for
                        // mensagem a informar que ficheiro foi lido com sucesso
                        System.out.println("Ficheiro lido com sucesso!");
                        // coloca flag a true indicar que ficheiro foi lido
                        leuMatriz = true;
                    } // end if
                    else{
                        System.out.println("Ficheiro inválido!\nA ordem da matriz não corresponde aos valores presentes no ficheiro!");
                    }
                } // end else
            } // end while
            // fecha o ficheiro
            fin.close();        
        } // end else
        
        // verifica se matriz do ficheiro é invertível
        if(calculaDeterminante(matrizCod)==0 && leuMatriz == true){
            System.out.printf("No entanto, a matriz introduzida pelo ficheiro %s não é invertível!", nomeFichCsv);
            System.out.println("\nAltere este ficheiro, escolha outro ficheiro ou introduza a matriz pelo teclado\n");
            // faz "reset" à matriz de forma a não se ter uma variável global com uma matriz não invertível
            resetMatriz();
        }
        else
            imprimeMatriz(matrizCod, ordem, ordem);
    } // end lerMatrizCsv
    
    
    /**
     * método para gerar matriz aletoriamente
     */
    public static void gerarMatriz(){
        // faz reset da matriz
        resetMatriz();
        // flag para verificar se matriz é invertivel
        boolean invertivel = false;
        // chama método para pedir ordem da matriz
        pedirOrdemMatriz();
        // pede ao utilizador o valor máximo possível
        System.out.println("Introduza o valor máximo possível:");
        int max = ler.nextInt();
        // impede que programa crashe
        ler.nextLine();
        // gerar matriz aleatória
        
        while(!invertivel){
            for(int i = 0; i < ordem; i++){
                for(int j = 0; j < ordem; j++){
                    matrizCod[i][j] = gerarAleatorio(max);
                } // end for
            } // end for
        if(calculaDeterminante(matrizCod)!=0)
            invertivel = true;
        } // end while
        // linha de teste
        imprimeMatriz(matrizCod, ordem, ordem);
    } // end gerarMatriz()
    
    
    /**
     * método para pedir ao utilizador a ordem da matriz
     */
    public static void pedirOrdemMatriz(){
        // pedir ordem da matriz ao utilizador
        System.out.println("Introduza a ordem da matriz (entre 2 e 4)");
        String temp = ler.next();
        // avisa utilizador caso introduza algo diferente de um número
        while((temp.charAt(0) < '0' || temp.charAt(0) > '9') || temp.length() > 1){
            System.out.println("Por favor, certifique-se que introduz um valor para a ordem da matriz entre 2 e 4");
            temp = ler.next();   
        }    
        // com base na tabela ASCII, coverte char para inteiro respectivo
        ordem = temp.charAt(0) - 48;
        // força utilizador a colocar ordem entre 2 e 4
        while(ordem < 2 || ordem >= 5){
            System.out.println("Por favor, introduza um valor para a ordem da matriz entre 2 e 4");
            ordem = ler.nextInt();
        } // end while
        ler.nextLine();
    }
    
    
    /**
     * método para gerar número inteiro aleatório, entre 0 e max
     * @param max
     * @return 
     */
    public static int gerarAleatorio(int max){
        // número tem que ser positivo, logo mínimo tem que ser 0
        int min = 0;
        /* math.random() gera aleatorio entre 0 e 1, logo para obter número inteiro
         * entre 0 e o máximo definido pelo utilizador é necessário multiplicar pelo
         * valor maximo; é necessário fazer o cast deste double para int já que o valor 
         * a inserir na matriz tem que ser inteiro
         */
        int rand = (int) (Math.random() * max);
        return rand;
    }
    
    
    /**
     * método para calcular determinante da matriz
     * @return 
     */
    public static int calculaDeterminante(int[][] m){
        // declaração de variável para calcular determinante
        int det = 0;
        // define método usado para calcular determinante, com base na ordem da matriz
        if(ordem == 2){
            // chama regraPratica() se ordem == 2
            det = regraPratica(m);
        }
        else if(ordem == 3){
            // chama sarrus() se ordem == 3
            det = sarrus(m);
        }     
        else if(ordem == 4){
            // chama laPlace() se ordem == 4
            det = laPlace(m);
        }
        else
            // como a aplicação só admite matrizes de ordem 2, 3 ou 4, caso seja diferente é por que não existe nenhuma matriz no sistema
            System.out.print("Não existe nenhuma matriz carregada na aplicação!\nSeleccione opção 3 ou 4 para carregar matriz!\n");
        // linha de teste (apagar quando já não for necessário para testes)
//        System.out.println("Determinante: " + det);
        //retorna determinante
        return det;
    } // end calculaDerminante
    
    
    /**
     * método de cálculo do determinante com base na regra prática, apenas aplicável a matrizes
     * 2*2 
     * @return 
     */
    public static int regraPratica(int[][] m){
        return ((m[0][0] * m[1][1]) - (m[0][1] * m[1][0]));
    } // end regraPratica()
    
    
    /**
     * método para cálculo do determinante com base na regra de Sarrus, apenas
     * aplicável a matrizes 3*3
     * @param m matriz 3*3
     * @return 
     */
    public static int sarrus(int[][] m){
        return ((m[0][0] * m[1][1] * m[2][2]) + (m[0][1] * m[1][2] * m[2][0]) + (m[0][2] * m[1][0] * m[2][1]) - (m[0][1] * m[1][0] * m[2][2]) - (m[0][0] * m[1][2] * m[2][1]) - (m[0][2] * m[1][1] * m[2][0]));
    } // end sarrus()
    
    
    /**
     * método de LaPlace para calcular determinante de uma matriz 4*4; baseia-se
     * na geração de várias matrizes 3*3 e do recurso ao métodos de Sarrus para
     * calcular o determinante de cada uma delas
     * @return 
     */
    public static int laPlace(int[][] m){
        // declarar variável para guardar valor do determinante (inicializada a zero para se poder somar nas várias iteracções do ciclo)
        int det = 0;
        // declarar matriz temporaria 3*3 para usar em sarrus()
        int[][] temp = new int[3][3];
        // chamar sarrus() num total de 4x
        for(int a = 0; a < 4; a++){
            // preencher matriz temporária
            preencherMatrizTemp(temp,a);
            // calcula determinante para cada matriz temp e acumula em det (teorema de LaPlace)
            det += m[3][a] * Math.pow(-1, 4 + (a+1)) * sarrus(temp);
        }
        // devolve determinante da matriz 4*4
        return det;
    } // end laPlace
    
    
    /**
     * método para preencher matriz temporária 3*3 a partir de uma matriz 4*4,
     * necessário para laPlace
     * @param m matriz temporaria 3*3
     * @param a indica coluna a não ser copiada para a matriz
     */
    public static void preencherMatrizTemp(int[][] m, int a){
        /* ignorar linha 4 é alcançado com o for a correr até apenas <3, isto é,
         * assim só percorre as linhas 0, 1 e 2 da matriz 4*4:
         * no método de laPlace é necessário ignorar uma linha e
         * neste caso optou-se por ignorar sempre a última linha; são necessárias
         * duas variaveis: i (para controlar linha da matriz 4*4) e k (para controlar
         * linha da matriz 3*3) */ 
        for(int i = 0, k = 0; i < 3; i++, k++){ // tanto i como k podem ser incrementados no ciclo exterior já que a linha a eliminar é sempre a mesma
            for(int j = 0, l = 0; j < 4; j++){ // apenas a coluna da matriz 4*4 é incrementada
                // quando a coluna é igual à que é passada por parâmetro não se copiam os valores
                if(j != a){
                    // quando a coluna é diferente da q é passada por parâmetro pode-se copiar os valores
                    m[k][l] = matrizCod[i][j];
                    // apenas se pode incrementar a coluna da matriz 3*3 quando existe atribuição do valor da 4*4 para esta matriz
                    l++;
                } // end if
            } // end for
        } // end for
    } // end preencherMatrizTemp
    
    
    /**
     * método de reset da matriz, isto é, volta a colocar a matriz com todos os 
     * valores a -1
     */
    public static void resetMatriz(){
        for(int i = 0; i < ordem; i++){
            for(int j = 0; j < ordem; j++)
                matrizCod[i][j] = -1;
        }
    } // end resetMatriz
    
    
    /**
     * método para ler tabela conversora a partir do teclado, recorre aos arrsys
     * de char e int declarados como variaveis globais
     */
    public static void lerTabelaConvTeclado(){
        // chama método para apagar tabela conversora
        boolean op = apagarTabelaConversora();
        // caso utilizador prentenda continuar apaga os valores nos 2 arrays e lê dados introduzidos
        if(op == true){
            // flag que controlar ciclo de introdução
            boolean terminar = false;
            // string para guardar valores lidos pelo teclado
            String par;
            // variável contadora
            int cont = 0;
            System.out.println("Introduza a tabela conversora de caracteres. \nUse pares de caracter-código separados por - \n(por exemplo, A-1). \nEscreva END quando terminar");
            // ciclo para introdução da tabela
            while(!terminar){
                // início de try catch para verificar que utilizador introduz par correcto
                try{              
                    par = ler.nextLine();
                    // se utilizador não introduz "end" lê os dados inseridos
                    if(!par.equalsIgnoreCase("end")){
                        // se char não existe no vector então pode introduzir
                        if(verificaRepeticaoChar(tabelaConvChars,par.charAt(0)) == true){ 
                            System.out.println("Erro: o caracter já existe! Tente novamento ou END para terminar!");
                        } // end if
                        else{
                            // guarda código numérico em temp
                            String temp = par.substring(2,par.length());
                            int tempInt = Integer.parseInt(temp);
                            if(verificaRepeticaoInt(tabelaConvCodigo,tempInt) == true || tempInt <= 0){
                                System.out.println("Erro: O código já existe ou então introduz um código inválido! Tente novamento ou END para terminar!");
                            } // end if
                            else{
                                // se passou nas verificações atribui código aos respectivos arrays
                                tabelaConvChars[cont] = par.charAt(0);
                                tabelaConvCodigo[cont] = tempInt;
                                // incrementa contador introduzir restantes dados nas posições correctas do vector
                                cont++;
                                System.out.printf("Leitura com sucesso! Digite novo par caracter-código ou END para terminar!\n(sistema ainda suporta %d códigos)\n", (200-cont));  
                            } // end else     
                        } // end else     
                    } // end if
                    else{
                        // caso o utilzador introduza "end", flag passa a true para sair do ciclo
                        terminar = true;
                    } // end else
                } // end try  
                catch(Exception exc){
                    System.out.println("Erro na leiura!\nCertifique-se que introduz um par caracter-código separado por - \n(por exemplo, A-1). \nEscreva END quando terminar");
                } // end catch
            } // end while  
        } // end if
    } // end lerTabelaConvTeclado     
        
    
    /**
     * método para verificar se um dado caracter já existe num array de caracters
     * @param vec vector de chars
     * @param c char a procurar
     * @return true, se char existe no array, false se não existir
     */
    public static boolean verificaRepeticaoChar(char[] vec, char c){
        boolean flag = false;
        int i = 0;
        for(; i < vec.length; i ++){
            if(vec[i] == c)
                flag = true;
        } // end for
        return flag;
    } // end verificaRepeticaoChar
    
    
    /**
     * método para verificar se um dado inteiro já existe num array de inteiros
     * @param vec array de inteiros
     * @param a inteiro a procurar
     * @return 
     */
    public static boolean verificaRepeticaoInt(int[] vec, int a){
        boolean flag = false;
        int i = 0;
        for(; i < vec.length; i ++){
            if(vec[i] == a)
                flag = true;
        } // end for
        return flag;
    } // end verificaRepeticaoInt
    
    
   /**
    * método para apagar tabela conversora de caracteres; apenas apaga se utilizador
    * confirmar essa opção
    * @return true (apaga tabela), false (não apaga)
    */
    public static boolean apagarTabelaConversora(){
        System.out.println("Ao ler uma nova tabela conversora TODOS os dados da tabela actual serão apagados!\nDeseja continuar? \n[S] Sim\n[N] Não");
        String simNao = ler.nextLine().toLowerCase();
        char op = simNao.charAt(0);
        // verifica se opção introduzida é válida
        while((op != 's' && op != 'n') || simNao.length() > 1){
            System.out.println("Opção inválida! Tente novamente");
            simNao = ler.nextLine().toLowerCase();
            op = simNao.charAt(0);
        } // end while
        // se opção for sim apaga tabela
        if(op == 's'){
            for(int i = 0; i < tabelaConvChars.length; i++){
                tabelaConvChars[i] = 0;
                tabelaConvCodigo[i] = 0;
            } // end for
            return true;
        } // end if
        return false;
    } // end apagaTabelaConversora
    
    
    /**
     * método para ler tabela conversa de caracteres a partir de um ficheiro no
     * formato de csv
     */
    public static void lerTabelaConvCsv() throws Exception{
        // chama método para apagar tabela conversora
        boolean op = apagarTabelaConversora();
        // caso utilizador prentenda continuar apaga os valores nos 2 arrays e lê dados introduzidos
        if(op == true){
            // pede ao utilizador o nome do ficheiro
            System.out.println("Indique o nome do ficheiro que contém a tabela conversora, incluindo a extensão");
            System.out.println("(exemplo:tabelaConv.csv)");
            String nomeTabelaCsv = ler.nextLine();
            // declaração de variável do tipo ficheiro
            File f = new File(nomeTabelaCsv);
            // verifica se ficheiro existe
            if(!f.exists())
                System.out.println("O ficheiro indicado não existe!");
            // verifica se ficheiro tem conteúdo
            else if(f.length() == 0)
                System.out.println("O ficheiro indicado não tem conteúdo!");
            else{
                // objecto do tipo scanner para ler o ficheiro
                Scanner fin = new Scanner(f);
                // ler conteúdo do ficheiro
                while(fin.hasNextLine()){
                    String temp = fin.nextLine();
                    // array temporário para guardar valores
                    // cada posição do array fica, por exemplo "A-1"
                    String[] arrayTemp = temp.split(";");
                    // percorrer todo o arrayTemp
                    for(int i = 0; i < arrayTemp.length; i++){
                        // atribui dados do ficheiro aos respectivos arrays
                        // o char corresponde ao índice 0 da string
                        tabelaConvChars[i] = arrayTemp[i].charAt(0);
                        // o código corresponde à substring que vai do índice 2 até ao comprimento dessa string
                        tabelaConvCodigo[i] = Integer.parseInt(arrayTemp[i].substring(2, arrayTemp[i].length()));
                    } // end for
                } // end while
                fin.close();
                System.out.println("Tabela conversora lida com sucesso!!");
            } // end else
        } // end if  
    } // end lerTabelaConvCsv
    
    
    /**
     * método que permite codificar a mesnagem e mostra-la no ecra sob a forma 
     * de matriz
     */
    public static void mostrarMsgCodificada() throws FileNotFoundException{
        // verificar se existe mensagem, matriz ou tabela codificadora
        boolean msg = existeMensagem(mensagem); 
        boolean matriz = existeMatrizCod(ordem);
        boolean tabela = existeTabelaConv(tabelaConvChars, tabelaConvCodigo);
        boolean tabelaSuf = tabelaConvSuf();
        System.out.println(tabelaSuf);
        // se não existir algum dos elementos anteriores, avisa o utilizador
        if(!msg || !matriz || !tabela || !tabelaSuf){
            System.out.println("Não é possível prosseguir! Motivo(s): ");
            if(!msg)
                System.out.println("Não existe mensagem para codificar!");
            if(!matriz)
                System.out.println("Não existe matriz codificadora!");
            if(!tabela)
                System.out.println("Não existe tabela conversora!");
            if(!tabelaSuf)
                System.out.println("A tabela conversora não tem caracteres suficientes para codificar a mensagem!");
        } // end if
        // se passar em todas as condições então passa para a codificação da mensagem
        else{
            // chama método para ajustar mensagem em funcao da matriz codificadora
            ajustaMensagem();
            // converte mensagem para código numérico com base na tabela conversora
            int[] msgConvertida = converteMsgParaNum();
            // cria array para colocar a mensagem convertida, com tamanho adequado, em que nº linhas = ordem e nº colunas = comprimento da mensagem / ordem
            int[][] msgMatriz = new int[ordem][mensagem.length()/ordem];
            // chama método para preencher msgMatriz
            msgMatriz = msgConvertidaParaMatriz(msgConvertida,ordem,mensagem.length()/ordem);
            // array que vai receber produto da matriz codificador * msgMatriz; o seu nº de linhas = ordem e nº colunas = colunas da msgMatriz
            int[][] produto = new int[ordem][mensagem.length()/ordem];
            // chama método para multiplicar as matrizes
            produto = multiplicaMatrizes(matrizCod,msgMatriz,ordem,mensagem.length()/ordem);
            // guarda valores da matriz produto (i.e., a mensagem codificada) num ficheiro txt
            escreveMsgCodificadaEmTxt(produto);
            // imprime mensagem codificada na forma de matriz
            imprimeMatriz(produto,ordem,mensagem.length()/ordem); 
        } // end else
    } // end mostrarMsgCodifica()
    
    
    /**
     * método para verificar se existe mensagem no sistema
     * @param mensagem
     * @return true se existir, false se não existir
     */
    public static boolean existeMensagem(String m){
        boolean b = false;
        if(!m.equals(""))
            b = true;
        return b;
    } // end existeMensagem
    
  
    /**
     * método para verificar se existe matriz codificante, com base no valor 
     * da variável global ordem
     * @param o
     * @return true se existir, false se não existir
     */
    public static boolean existeMatrizCod(int o){
        boolean b = false;
        if(o > 1 && o < 5)
            b = true;
        return b;
    }
    
    
    /**
     * método que verifica se existe tabela conversora de caracteres, com base
     * no primeiro elemento de cada array
     * @return true se existir, false se não existir
     */
    public static boolean existeTabelaConv(char[] c, int[] i){
        boolean b = false;
        if(!(c[0] == 0 || i[0] == 0))
            b = true;
        return b;
    }
    
    
    /**
     * método que verifica se a tabela de conversão de caracteres tem todos os 
     * caracteres necessários para codificar a mensagem
     * @return true, se for suficiente; false se não for
     */
    public static boolean tabelaConvSuf(){
        char[] vec = msgToCharArray();
        for(int i = 0; i < vec.length; i++){
           boolean existe = false;
           for(int j = 0; j < tabelaConvChars.length; j++){
               if(vec[i] == tabelaConvChars[j])
                   existe = true;
            } // end for
           if(!existe)
               return false;
        } // end for
        return true;
    } // end tabelaConvSuf
    
    
    /**
     * método que converte string que contem a mensagem num array de chars
     * @return vector de chars
     */
    public static char[] msgToCharArray(){
        char[] msgChar = new char[mensagem.length()];
        // colocar mensagem num vector de chars
        for(int i = 0; i < msgChar.length; i++){
            msgChar[i] = mensagem.charAt(i);
        } // end for
        return msgChar;
    } // end msgToCharArray
    
    
    /**
     * método para converter mensagem de texto para numérico, com base na tabela
     * de conversão de caracteres
     */
    public static int[] converteMsgParaNum(){
        // declarar array de char onde vai ser colocada a mensagem, caracter a caracter
        char[] msgChar = msgToCharArray();
        // declarar vector que vai guardar mensagem na forma numérica
        int[] msgInt = new int[mensagem.length()];
        // colocar em vec os respectivos códigos com base na tabela codificadora
        // percorre o comprimento do vector de int onde se vai guardar msg convertida em int
        for(int i = 0; i < msgInt.length; i++){
            // percorrer array de chars e códigos à procura do código respectivo
            for(int j = 0; j < tabelaConvChars.length; j++){
                // quando encontra o respectivo char, atribui o código respectivo
                if(msgChar[i] == tabelaConvChars[j])
                    msgInt[i] = tabelaConvCodigo[j];
            } // end for
        } // end for     
        // end teste
        return msgInt;
    } // end converteMsgParaNum
    
    
    /**
     * metodo que acresenta espacos em branco, se necessario, a mensagem original
     */
    public static void ajustaMensagem(){
        // se a matriz codificadora tiver ordem 2 ou 4, a mensagem tem que ter comprimento par
        if(ordem == 2 || ordem == 4){
            // se comprimento da mensagem for impar é necessário acrescentar um espaço vazio ao fim da mensagem
            if(mensagem.length() % 2 != 0)
                mensagem += " ";
        } // end if
        // se matriz codificador tiver ordem 3, vai acrescentando espaços em branco até que comprimento da mensagem seja divisel por 3
        else{
            int temp = mensagem.length() % 3;
            while(temp != 0){
                mensagem += " ";
                temp = mensagem.length() % 3;
            } // end while
        } // end else
    } // end ajustaMensagem
    
    
    /**
     * passa os valores num array de int para um array 2D de int
     * @param m array com uma dimensão
     * @param l número de linhas do array a devolver
     * @param c número de colunas do array a devolver
     * @return array 2D
     */
    public static int[][] msgConvertidaParaMatriz(int[] m, int l, int c){
        // variável contadora que controla a posição do array 1D
        int cont = 0;
        // declaração de array 2D com nº de linhas e colunas iguais às passadas por parâmetro
        int[][] temp = new int[l][c];
        // percorrer todo o array 2D
        for(int i = 0; i < l; i++){
            for(int j = 0; j < c; j++){
                // atribuir à respectiva posição do array 2D o elemento do array 1D
                temp[i][j] = m[cont];
                cont++;
            }
        }
        // devolve array 2D
        return temp;
    }
    
    
    /**
     * método para calcular o produto entre duas matrizes m1 e m2
     * @param m1 matriz
     * @param m2 matriz
     * @param l número de linhas da matriz produto
     * @param c número de linhas da matriz produto
     * @return matriz resultante do produto de m1 por m2
     */
    public static int[][] multiplicaMatrizes(int[][] m1, int[][] m2, int l, int c){
        int[][] produto = new int[l][c];
        // percorrer todas as posições da matriz produto
        for(int i = 0; i < l; i++){
            for(int j = 0; j < c; j++){
                int[] temp1 = new int[l];
                int[] temp2 = new int[l];
                // colocar valores em temp1 e temp2
                for(int a = 0; a < l; a++){
                    temp1[a] = m1[i][a];
                    temp2[a] = m2[a][j];
                } // end for
                // calcular produto na posição [i][j] da matriz produto
                produto[i][j] = somatorio(temp1, temp2, l);
            } // end for
        } // end for
        return produto;
    } // end multiplicaMatrizes
    
    
    /**
     * calcula o somatorio do produto dos elementos de cada array, de i=0 ate
     * ao comprimento máximo do array
     * @param linha array 1D
     * @param coluna array 1D
     * @param l total de elementos a percorrer
     * @return somatorio do produto dos elementos
     */
    public static int somatorio(int[] linha, int[] coluna, int l){
        int somatorio = 0;
        for(int i = 0; i < l; i ++){
            somatorio += linha[i] * coluna[i];
        } // end for
        return somatorio;
    } // end somatorio
    
    
    /**
     * método que retorna a matriz codificante
     * @return 
     */
    public static int[][] getMatrizCod(){
        return matrizCod;
    } // end getMatrizCod
    
    
    /**
     * método que retorna o nº de linhas (logo, de colunas também) que a variável
     * global matrizCod está a utilizar
     * @return 
     */
    public static int matrizValida(){
        int cont = 0;
        for(int i = 0; i < 4; i ++){
            if(matrizCod[i][0] == -1)
                break;
            cont++;
        }
        return cont;
    }
    
    
    /**
     * método que retorna array com informação dos caracteres usados na tabela
     * de conversão de caracteres
     * @return 
     */
    public static char[] getTabelaConvChars(){
        return tabelaConvChars;
    } // end getTabelaConvChars
    
    
    /**
     * método que retorna array com informação dos códigos inteiros usados na tabela
     * de conversão de caracteres
     * @return 
     */
    public static int[] getTabelaConvCodigo(){
        return tabelaConvCodigo;
    }

    
    /**
     * método que determina o total de códigos que estão a ser utilizados pela 
     * tabela conversora de caracterse
     * @return 
     */
    public static int tabelaValida(){
        int cont = 0;
        for(int i = 0; i < 200; i ++){
            if(tabelaConvCodigo[i] == 0)
                break;
            cont++;
        } // end for
        return cont;
    }
    
    
    /**
     * escreve a mensagem codificada num ficheiro de texto
     * @param m matriz 2D que contem a mensagem codificada
     * @throws FileNotFoundException 
     */
    public static void escreveMsgCodificadaEmTxt(int[][] m) throws FileNotFoundException{
        Formatter fout = new Formatter(new File("mensagemCodificada.txt"));
        // percorre todo o array que tem a mensagem codificada e escreve no ficheiro
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[0].length; j++){
                fout.format("%d" + " ", m[i][j]);
            }
        }
        fout.close();
        // passa variavel que controla se foi codificada msg a true
        existeMsg = true;
    } // end escreveMsgCodificadaEmTxt
    
    
} // end class Emissor
    
    
  