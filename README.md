LAPR1
=====

Academic project in the scope of "Laboratory/Project 1" (1st year of Informatics Engineering). 
This is a console-based Java application that consists in two main applications, one responsible for the encryption of 
a text message, and other capable to read the encrypted message. The encryption alghorithm is based in the algaebric 
properties of invertable matrixes. 
